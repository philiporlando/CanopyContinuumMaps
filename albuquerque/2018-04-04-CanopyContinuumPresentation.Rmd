---
title: <font size='14'>PurpleAir Site Selection</font>
subtitle: <font size='12'>Canopy Continuum</font>
author: <font size='10'>Philip Orlando & Linda George</font>
date: <font size = '10'>April 4, 2018</font>
output:
  revealjs::revealjs_presentation:
    theme: "black"
    transition: slide
    self_contained: false
    reveal_plugins: ["zoom"]
---

```{r}
```


## Spatial Heterogeneity of PurpleAir Network

-What is the optimal sensor placement?
  
  population density
  
  traffic variables
  
  urban canopy


-At what distance do nearby sensors begin to disagree?

```{r}
```


##

<img data-src="./figures/2018-03-22_R_Squared_Distance_geom_point_1km.png">


```{r}

```



##

<img data-src="./figures/2018-03-22_R_Squared_Distance_geom_point_2km.png">


```{r}

```


##

<img data-src="./figures/2018-03-22_R_Squared_Distance_geom_point_total.png">


```{r}

```



##
<h4>Albuquerque 2 km<sup>2</sup> grid:</h4>
<div style="z-index:9999999;" class="stretch">
<iframe class="stretch" style="height:100%;width:100%; z-index:999999999;" data-src="./maps/albuq_prelim_ID.html"></iframe>
</div>
<p>n = 244</p>



```{r}
```

##

<h4>Random sample:</h4>
<div style="z-index:9999999;" class="stretch">
<iframe class="stretch" style="height:100%;width:100%; z-index:999999999;" data-src="./maps/albuq_prelim_ID_sample.html"></iframe>
</div>
<p>   <font size="4">Above 70<sup>th</sup> percentile, n = 18</font></p>
<p> <font size="4">Below 70<sup>th</sup> percentile, n = 12</font></p>



```{r}
```

##
<h4>Sacramento 2 km<sup>2</sup> grid:</h4>
<div style="z-index:9999999;" class="stretch">
<iframe class="stretch" style="height:100%;width:100%; z-index:999999999;" data-src="./maps/sacra_prelim_ID.html"></iframe>
</div>
<p>n = 425</p>



```{r}
```

##
<h4>Boise 2 km<sup>2</sup> grid:</h4>
<div style="z-index:9999999;" class="stretch">
<iframe class="stretch" style="height:100%;width:100%; z-index:999999999;" data-src="./maps/boise_prelim_ID.html"></iframe>
</div>
<p>n = 232</p>


```{r}
```

##
<h4>Tacoma 2 km<sup>2</sup> grid:</h4>
<div style="z-index:9999999;" class="stretch">
<iframe class="stretch" style="height:100%;width:100%; z-index:999999999;" data-src="./maps/tacoma_prelim_ID.html"></iframe>
</div>
<p>n = 303</p>
<p>   <font size="4">*clipping by Pierce County since Tacoma is contained by Seattle's urban area.</font></p>

</body>

```{r}
```
