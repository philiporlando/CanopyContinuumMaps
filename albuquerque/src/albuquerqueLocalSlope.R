# created by Philip Orlando @ Sustainable Atmopsheres Research Lab
# Canopy Continuum Project, USFS
# PI Dr. Linda George
# 2018-08-08
# determine local slopes for Albuquerque

# set up environment ---------------------------------------------------------------

# load the necessary packages
if (!require(pacman)) {
  install.packages("pacman")
  library(pacman)
}

p_load(readr
       ,readxl
       #,xlsx
       ,ggplot2
       ,plyr
       ,dplyr
       ,broom
       ,reshape2
       ,tidyr
       ,stringr
       ,magrittr
       ,rlist
       ,grid
       ,gridExtra
       ,tidyquant
       ,scales
       ,qdapRegex
       ,sp
       ,sf
       #,mapview # ***** with specific leaflet install
       ,devtools
       ,RColorBrewer
       ,classInt
       ,htmltools
       ,scales
       ,htmlwidgets
       ,httr
       ,jsonlite
       ,rgdal
       ,pbapply
       ,snow
       ,parallel
       ,data.table
       ,RPostgres
       ,tsoutliers
       ,forecast
)

# define global variables -------------------------------------------------------------------

## connecting to local db
host <- 'pgsql120.rc.pdx.edu'
db <- 'canopycontinuum'
user <- 'porlando'
port <- 5433
pw <- scan("../batteries.pgpss", what = "") # in parent dir


# open connection to our db
con <- dbConnect(drv = RPostgres::Postgres()
                 ,dbname = db
                 ,host = 'pgsql102.rc.pdx.edu' # not sure why object host isn't working...
                 ,port = port
                 ,password = pw
                 ,user = user)


# create unanimous time resolution for all data
time_resolution <- "1 sec"

# time zone applied to all data
time_zone <- "US/Pacific"

# CRS
wgs_84 <- "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs "


# Albuquerque, NM, UTM 13N
utm_13N <- "+proj=utm +zone=13 +ellps=WGS84 +datum=WGS84 +units=m +no_defs "

# Sacramento, CA, UTM 10S, meters
epsg_26911 <- "+proj=utm +zone=10 +ellps=GRS80 +datum=NAD83 +units=m +no_defs"

# Oregon North NAD83 HARN meters
epsg_2838 <- "+proj=lcc +lat_1=46 +lat_2=44.33333333333334 +lat_0=43.66666666666666 +lon_0=-120.5 +x_0=2500000 +y_0=0 +ellps=GRS80 +units=m +no_defs "

# Oregon North NAD83 Meters UTM Zone 10
epsg_26910 <- "+proj=utm +zone=10 +ellps=GRS80 +datum=NAD83 +units=m +no_defs "

# Function definition ------------------------------------------------------------------

# creating a custon not-in function
'%!in%' <- function(x,y)!('%in%'(x,y))

# read function for the 5-min data that Meena sent over!
read_frm <- function(file_path) {
  
  # read in the raw data
  x <- read.csv(file_path, header = TRUE, skip = 2)
  
  # delete sub headers
  x <- x[-c(1:2),]
  
  # convert date from character to POSIXct
  x$Date...Time <- as.POSIXct(x$Date...Time, format = "%m/%d/%Y %H:%M %p")
  x <- x %>% na.omit()
  
  # convert to tidy format, gather on site id
  long <- x %>%
    gather(id, pm2_5_atm, -c(Date...Time))
  
  # convert characters in pm2_5 data to NA
  long$pm2_5_atm <- as.numeric(long$pm2_5_atm)
  
  
  # exclude NAs 
  long <- long %>% na.omit()
  
  colnames(long) <- c("datetime"
                      ,"site_id"
                      ,"pm2_5_ref")
  long$site_id <- gsub("\\.", " ", long$site_id)
  #long$site_id <- stringr::str_replace(long$site_id, ".", " ")
  
  
  return(long)
  
  
}

# 5-min resolution required for overlapping A & B sensors...
# time_resolution <- "3600 min"
# time_step <- 3600 # same as time res without string "min"


# for clean 5-min breaks
align.time.down = function(x,n) {
  index(x) = index(x) - n
  align.time(x,n)
}

# data processing -------------------------------------------------------------------------

## joining the purpleair and frm data

# this file just went missing over my break...
# pa <- read.csv("./data/output/2018-07-20_17_14-thingspeak-5min.txt" 
#                ,stringsAsFactors = FALSE
#                ,na.strings = c("", " ", "NA"))

# pa_fast <- fread("./data/output/2018-07-19-thingspeak-5min-v3.txt", stringsAsFactors = FALSE
#                  ,showProgress = TRUE
#                  ,na.strings = c("", " ", "NA"))

#pa <- pa_fast

# pull in data from database instead:

# this isn't working because of the simple feature class column not being present... even after alter table in postgis...
pa <- st_read(dsn = con
              ,geom_column = "geom" # no simple feature geometry column present in database...
              ,table = "abq_local_slope"
              ,query = "SELECT * FROM abq_local_slope;"
              #,EWKB = TRUE
)

# this works for now...
pa <- dbReadTable(conn = con
                  ,name = "abq_local_slope")

# figure this out later
pa$geom <- st_as_sfc(pa$geom)
pa <- st_as_sf(pa)

# read in the frm data
frm_path <- "./data/frm/portland_5stn_20180101_to_20180719.csv"
frm <- read_frm(frm_path)

# make sure the datetime columns match
names(pa)[names(pa) == "created_at"] <- "datetime"

# pa$datetime <- strptime(pa$datetime, format = "%Y-%m-%d %H:%M:%S")
pa$datetime <- lubridate::ymd_hms(pa$datetime)
# getting failed to parse warning, called the same function again and no warnings occur... 
# pa$datetime <- lubridate::ymd_hms(pa$datetime) # causing more problems later?
## is the vector too big to be processed?


pa$temp_f <- as.numeric(pa$temp_f)
pa$humidity <- as.nuimeric(pa$humidity)

pa$pm1_0_atm <- as.numeric(pa$pm1_0_atm)
pa$pm2_5_atm <- as.numeric(pa$pm2_5_atm)
pa$pm10_0_atm <- as.numeric(pa$pm10_0_atm)

pa$pm1_0_cf_1 <- as.numeric(pa$pm1_0_cf_1)
pa$pm2_5_cf_1 <- as.numeric(pa$pm2_5_cf_1)
pa$pm10_0_cf_1 <- as.numeric(pa$pm10_0_cf_1)

pa$p_0_3_um <- as.numeric(pa$p_0_3_um)
pa$p_0_5_um <- as.numeric(pa$p_0_5_um)
pa$p_1_0_um <- as.numeric(pa$p_1_0_um)
pa$p_2_5_um <- as.numeric(pa$p_2_5_um)
pa$p_5_0_um <- as.numeric(pa$p_5_0_um)
pa$p_10_0_um <- as.numeric(pa$p_10_0_um)

str(frm)
str(pa)

# checkthe time zone of pa data
attr(pa$datetime, "tzone")
attr(frm$datetime, "tzone")

# convert PurpleAir data from UTC to Local Time (US/Pacific)
attr(pa$datetime, "tzone") <- "US/Pacific"

# ensure the FRM data is on the correct time zone (blank by default)
attr(frm$datetime, "tzone") <- "US/Pacific"

# begin the join
df <- inner_join(pa, frm, by = c("datetime"))
df <- df[complete.cases(df), ]


# convert vectors to numeric
df$pm1_0_atm <- as.numeric(df$pm1_0_atm)
df$pm2_5_atm <- as.numeric(df$pm2_5_atm)
df$pm10_0_atm <- as.numeric(df$pm10_0_atm)

df$pm1_0_cf_1 <- as.numeric(df$pm1_0_cf_1)
df$pm2_5_cf_1 <- as.numeric(df$pm2_5_cf_1)
df$pm10_0_cf_1 <- as.numeric(df$pm10_0_cf_1)

df$p_0_3_um <- as.numeric(df$p_0_3_um)
df$p_0_5_um <- as.numeric(df$p_0_5_um)
df$p_1_0_um <- as.numeric(df$p_1_0_um)
df$p_2_5_um <- as.numeric(df$p_2_5_um)
df$p_5_0_um <- as.numeric(df$p_5_0_um)
df$p_10_0_um <- as.numeric(df$p_10_0_um)

df$pm2_5_ref <- as.numeric(df$pm2_5_ref)


# aggregate to desired time resolution (default is 5-min)
# df <- df %>%
#   group_by(datetime = cut(as.POSIXct(datetime
#                                      ,format = "%Y-%m-%d %H:%M:%S"
#                                      ,tz = "US/Pacific"), breaks = time_resolution)
#            ,label
#            ,sensor
#            ,id
#            ,site_id) %>%
#   summarise_if(is.numeric, mean)
# 
# df$datetime <- as.POSIXct(df$datetime)
# 
# # remove NAs
# df <- df %>% na.omit()

# apply regression for the cross-product of PA and FRM id

output_names <- c("start_time"
                  ,"end_time"
                  ,"sensor"
                  ,"ref"
                  ,"r_squared"
                  ,"slope"
                  ,"intercept"
                  ,"p_value")

output_df <- data.frame(matrix(ncol = length(output_names), nrow = 0))
colnames(output_df) <- output_names


times <- c("5 min", "60 min", "3600 min")

# run lm for different time resolutions
for (time in times) {
  
  
  # output file
  txt_path <- paste0("./data/output/", time, "/", Sys.Date(), strftime(Sys.time(), format = "_%H_%M_%S"), "_LocalSlopeSummaryTablePDX.txt")
  
  if (time != "5 min"){
    
    # aggregate to desired time resolution (default is 5-min)
    # only works for hourly, or daily breaks... 
    # use the xts() method for clean breaks e.g. 5, 10, 15 min etc.
    df_agg <- df %>%
      group_by(datetime = cut(as.POSIXct(datetime
                                         ,format = "%Y-%m-%d %H:%M:%S"
                                         ,tz = "US/Pacific"), breaks = time)
               ,label
               ,sensor
               ,id
               ,site_id) %>%
      summarise_if(is.numeric, mean)
    
    # re-convert datetime to POSIXct()
    df_agg$datetime <- as.POSIXct(df_agg$datetime)
    
    # remove NAs
    df_agg <- df_agg %>% na.omit()
    
  } else {
    
    df_agg <- df
    
  }
  
  # unique(df_agg$label) is breaking.... too big? corrupt data? 
  for (sensor in unique(df_agg$label)) {
    
    #print('test1')
    if(is.null(sensor)) {
      print(paste(sensor, "is null! Skipping..."))
      next
    }
    
    #pa_sub <- subset(df_agg, label == sensor)
    pa_sub <- df_agg[df_agg$label == sensor,]
    
    
    if(nrow(pa_sub) <= 0) {
      print(paste("pa_sub is null for", sensor))
      next
    }
    
    #print('test2')
    
    for(ref in unique(pa_sub$site_id)) {
      
      
      #pa_ref <- subset(pa_sub, site_id == ref)
      pa_ref <- pa_sub[pa_sub$site_id == ref,]
      
      if (nrow(pa_ref) <=0) {
        print(paste("pa_ref is null"))
        next
      }
      
      #print('test3')
      pa_ref <- pa_ref %>% na.omit()
      pa_ref <- pa_ref %>% arrange(datetime)
      
      start_time <- head(pa_ref$datetime, n = 1)
      end_time <- tail(pa_ref$datetime, n = 1)
      
      # change this as needed!
      upper_limit <- 100 # apply conditionals for specific pm categories!
      lower_limit <- 0 # omit negative values
      
      df_mod <- pa_ref[pa_ref$pm2_5_ref <= upper_limit | pa_ref$pm2_5_ref >= lower_limit, ]
      
      if(nrow(df_mod) <= 0) {
        print(paste("df_agg_mod is null"))
        next
      }
      
      # depreceated...
      # # filter outliers from FRM data
      # df_mod <- df_mod %>%
      #   filter(pm2_5_ref < 10^4) # remove data above 10 mg/m3
      
      
      # handle outliers more intelligently (if I can even use this word...)
      
      # handle temp and humidity outliers?
      
      # convert df to ts object
      ts_x <- ts(df_mod$pm2_5_ref)
      ts_y <- ts(df_mod$pm2_5_atm)
      
      # flag outliers
      out_x <- forecast::tsoutliers(ts_x)
      out_y <- forecast::tsoutliers(ts_y)
      
      # remove outliers from each sensor vector
      if (length(out_x$index) > 0) {
        df_mod <- df_mod[-out_x$index, ]
      }
      
      if (length(out_y$index) > 0) {
        df_mod <- df_mod[-out_y$index, ]
      }
      
      # remove NA, and NaN before calling lm()
      df_mod <- df_mod[complete.cases(df_mod),] %>% na.omit()
      
      # daily averages need to be darker because they're are fewer points
      if (time == "3600 min") {
        a <- 0.5
      } else {
        a <- 0.05
      }
      
      # add number of observations to plot title
      df_n <- nrow(df_mod)
      
      #print('test4')
      # define our regression plotting function
      ggregression <- function (fit) {
        
        ggplot(fit$model, aes_string(x = names(fit$model)[2], y = names(fit$model)[1])) + 
          geom_point(alpha = a) +
          geom_smooth(method = "lm", col = "red", level = 0.95, se = FALSE) + ## "lm" or "loess" fit!
          geom_abline(intercept = 0, slope = 1, linetype = 2, color = "firebrick") +
          theme_bw() + 
          xlab(names(fit$model)[2]) + 
          scale_x_continuous(limits = c(lower_limit, upper_limit)) +
          scale_y_continuous(limits = c(lower_limit, upper_limit)) +
          ylab(expression(~PurpleAir~mu*g*m^-3)) +
          #ylab(as.character(unique(df_mod$pollutant))) + 
          xlab(expression(~ODEQ~mu*g*m^-3)) + 
          #ylab(substitute(paste(foo, " ", mu, "", g, "", m^-3), list(foo = species))) + 
          #xlab(substitute(paste(foo, " ", mu, "", g, "", m^-3), "DustTrak")) + 
          theme(plot.title = element_text(hjust = 0.5, size = 15, face = "bold"),
                plot.subtitle = element_text(hjust = 0.5, size=12, face = "bold"), legend.position = "none",
                axis.text = element_text(size=rel(1.0), face = "bold", colour = "black"),
                axis.title = element_text(size=15, face = "bold")) +  
          labs(title = paste0(sensor, " & ", ref, " • ", as.Date(start_time), " • ", as.Date(end_time), " • ", time, "• n = ", df_n),
               subtitle = paste("Adj R2 = ", signif(summary(fit)$adj.r.squared, 4),
                                "Intercept =",signif(fit$coef[[1]], 2), 
                                " Slope =",signif(fit$coef[[2]], 2), 
                                " P =",signif(summary(fit)$coef[2,4], 3)))
      }
      
      
      print(paste("Trying", sensor, ref, start_time, end_time, time))
      
      mod <- lm(pm2_5_atm~pm2_5_ref, data = df_mod)
      r_squared <- signif(summary(mod)$r.squared, 4)
      slope <- signif(mod$coefficients[[2]], 2)
      jintercept <- signif(mod$coefficients[[1]], 2)
      
      if(is.na(r_squared)) {
        print("Data is corrupt, skipping...")
        next
      }
      
      
      # assign NA to p_value if try() returns an NA
      p_value <- try(signif(summary(mod)$coef[2,4], 3))
      if("try-error" %in% class(p_value)) {
        p_value <- NA
        next # need to break out of the loop to avoid the mod error
      }
      #print('test5')
      
      new_row <- list(start_time, end_time, sensor, ref, r_squared, slope, intercept, p_value)
      
      output_df <- rbind(output_df
                         ,data.frame(
                           start_time = start_time
                           ,end_time = end_time
                           ,sensor = sensor
                           ,ref = ref
                           ,r_squared = r_squared
                           ,slope = slope
                           ,intercept = intercept
                           ,p_value = p_value
                         ))
      
      
      # write output file or append to it
      if(!file.exists(txt_path)) {
        
        print(paste0("Creating file: ", basename(txt_path)))
        write.table(output_df
                    ,txt_path
                    ,row.names = FALSE
                    ,col.names = TRUE
                    ,quote = FALSE
                    ,sep = ",")
        
      } else {
        
        print(paste0("Appending file: ", basename(txt_path)))
        write.table(output_df
                    ,txt_path
                    ,row.names = FALSE
                    ,append = TRUE # append if already exists
                    ,col.names = FALSE
                    ,quote = FALSE # makes reading data a challenge if TRUE...
                    ,sep =  ",")
        
      }
      
      
      
      # plotting our regression results
      mod_plot <- ggregression(mod)
      plot(mod_plot)
      
      
      file_name <- paste0("./figures/lm/", time, "/", sensor, "_", ref, "_PDX.png")
      file_name <- str_replace_all(file_name, " ", "_")
      
      # R_squared are so bad, now saving all figures...
      if (r_squared >= 0.0) {
        
        if(!file.exists(file_name)) {
          
          print(paste("Saving plot for", start_time, end_time, sensor, ref))
          
          # ggsave is really slow at this DPI
          try(ggsave(filename = file_name,
                     plot = mod_plot,
                     scale = 1,
                     width = 16,
                     height = 10,
                     units = "in",
                     dpi = 400))
          #Sys.sleep(1) # is R tripping over itself?
        } else {
          
          print(paste("Figure already exists for", start_time, end_time, sensor, ref, time_resolution))
        }
      }
      
    }
    
    # cleaning up
    invisible(gc())
  }
  
  
  
  
}


